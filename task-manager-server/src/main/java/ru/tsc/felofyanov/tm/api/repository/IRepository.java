package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModelDTO> {

    void add(@Nullable M model);

    void addAll(@Nullable Collection<M> collection);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAllSort(@NotNull Sort sort);

    void clear();

    boolean existsById(@Nullable String id);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @Nullable
    M findOneById(@Nullable String id);

    M findOneByIndex(@Nullable Integer index);

    int remove(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    int removeById(@Nullable String id);

    int removeByIndex(@Nullable Integer index);

    int update(@NotNull M model);

    long count();
}


