package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.IProjectService;
import ru.tsc.felofyanov.tm.api.service.IProjectTaskService;
import ru.tsc.felofyanov.tm.api.service.ITaskService;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;

import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();

        @Nullable final TaskDTO task = taskService.findOneByIdUserId(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();

        @Nullable final TaskDTO task = taskService.findOneByIdUserId(userId, taskId);
        if (task == null) return;
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);

        taskService.removeAllByProjectId(userId, projectId);
        projectService.removeByIdByUserId(userId, projectId);
    }
}
