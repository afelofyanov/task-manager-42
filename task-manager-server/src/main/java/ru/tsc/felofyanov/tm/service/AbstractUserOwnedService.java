package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.IUserOwnerService;
import ru.tsc.felofyanov.tm.dto.model.AbstractWbsDTO;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.enumerated.Status;

import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractWbsDTO, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    public abstract M createByName(@Nullable String userId, @Nullable String name);

    @Nullable
    @Override
    public abstract M createByNameDescription(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Override
    public abstract void addByUserId(@Nullable final String userId, @Nullable final M model);

    @NotNull
    @Override
    public abstract List<M> findAllByUserId(@Nullable final String userId);

    @NotNull
    @Override
    public abstract List<M> findAllSortByUserId(@Nullable final String userId, @Nullable final Sort sort);

    @Override
    public abstract void clearByUserId(@Nullable final String userId);

    @Override
    public abstract boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    @Override
    public abstract M findOneByIdUserId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    @Override
    public abstract M findOneByIndexByUserId(@Nullable final String userId, @Nullable final Integer index);

    @Override
    public abstract int removeByUserId(@Nullable final String userId, @Nullable final M model);

    @Override
    public abstract int removeByIdByUserId(@Nullable final String userId, @Nullable final String id);

    @Override
    public abstract int removeByIndexByUserId(@Nullable final String userId, @Nullable final Integer index);

    @Override
    public abstract int update(@Nullable final M model);

    @NotNull
    @Override
    public abstract M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    @Override
    public abstract M updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    @Override
    public abstract M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @Override
    public abstract M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @Override
    public abstract long countByUserId(@Nullable final String userId);
}
