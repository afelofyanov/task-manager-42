package ru.tsc.felofyanov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.IAuthService;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.api.service.IUserService;
import ru.tsc.felofyanov.tm.dto.model.SessionDTO;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.LoginEmptyException;
import ru.tsc.felofyanov.tm.exception.field.PasswordEmptyException;
import ru.tsc.felofyanov.tm.exception.system.AccessDeniedException;
import ru.tsc.felofyanov.tm.exception.system.AuthenticationException;
import ru.tsc.felofyanov.tm.exception.system.LockException;
import ru.tsc.felofyanov.tm.util.CryptoUtil;
import ru.tsc.felofyanov.tm.util.HashUtil;

import java.util.Date;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    @Nullable
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.createWithEmail(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String json;
        try {
            json = CryptoUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDTO session = objectMapper.readValue(json, SessionDTO.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();

        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        @NotNull final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @Override
    public String login(final @Nullable String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @Nullable UserDTO user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new LockException();

        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptoUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }
}
