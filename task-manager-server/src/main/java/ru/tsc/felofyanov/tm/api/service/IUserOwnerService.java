package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.dto.model.AbstractWbsDTO;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.enumerated.Status;

import java.util.List;

public interface IUserOwnerService<M extends AbstractWbsDTO> extends IUserOwnerRepository<M>, IService<M> {

    @Nullable
    M createByName(@Nullable String userId, @Nullable String name);

    @Nullable
    M createByNameDescription(@Nullable String userId, @Nullable String name, @Nullable String description);

    void addByUserId(@Nullable String userId, @Nullable M model);

    @NotNull
    List<M> findAllByUserId(@Nullable String userId);

    @NotNull
    List<M> findAllSortByUserId(@Nullable String userId, @Nullable Sort sort);

    void clearByUserId(@Nullable final String userId);

    boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    M findOneByIdUserId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    M findOneByIndexByUserId(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    M updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    long countByUserId(@Nullable final String userId);
}
