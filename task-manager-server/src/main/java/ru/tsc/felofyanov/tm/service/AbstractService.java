package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.IService;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;
import ru.tsc.felofyanov.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModelDTO, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public abstract void add(@Nullable final M model);

    @Override
    public abstract void addAll(@Nullable Collection<M> models);

    @Override
    public abstract int update(@Nullable M model);

    @NotNull
    @Override
    public abstract List<M> findAll();

    @NotNull
    @Override
    public abstract List<M> findAllSort(@NotNull final Sort sort);

    @Override
    public abstract boolean existsById(@Nullable final String id);

    @NotNull
    @Override
    public abstract Collection<M> set(@NotNull Collection<M> models);

    @Override
    public abstract void clear();

    @Nullable
    @Override
    public abstract M findOneById(@Nullable final String id);

    @Override
    @Nullable
    public abstract M findOneByIndex(@Nullable final Integer index);

    @Override
    public abstract int remove(@Nullable final M model);

    @Override
    public abstract int removeById(@Nullable final String id);

    @Override
    public abstract int removeByIndex(@Nullable final Integer index);

    @Override
    public abstract void removeAll(@Nullable final Collection<M> collection);

    @Override
    public abstract long count();
}
