package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.AbstractWbsDTO;
import ru.tsc.felofyanov.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnerRepository<M extends AbstractWbsDTO> extends IRepository<M> {

    void addByUserId(@Nullable String userId, @Nullable M model);

    @NotNull
    List<M> findAllByUserId(@Nullable String userId);

    @NotNull
    List<M> findAllSortByUserId(@Nullable String userId, @Nullable Sort sort);

    void clearByUserId(@NotNull String userId);

    boolean existsByIdUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    int removeByUserId(@Nullable String userId, @Nullable M model);

    int removeByIdByUserId(@Nullable String userId, @Nullable String id);

    int removeByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    M updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    long countByUserId(@Nullable String userId);
}
