package ru.tsc.felofyanov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.ITaskService;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<TaskDTO, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(TaskIdEmptyException::new);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.removeAllByProjectId(userId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(@Nullable TaskDTO task) {
        if (task == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@Nullable Collection<TaskDTO> models) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO createByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @Nullable final TaskDTO task = new TaskDTO(userId, name);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO createByNameDescription(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @Nullable TaskDTO task = new TaskDTO(userId, name, description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public void addByUserId(@Nullable final String userId, @Nullable final TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.addByUserId(userId, model);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllSort(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllSort(sort);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllSortByUserId(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAllByUserId(userId);
        String test = sort.toString();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllSortByUserId(userId, sort);
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clearByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Override
    @SneakyThrows
    public boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneByIdUserId(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models) {
        if (models.isEmpty()) return models;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable final Collection<TaskDTO> result;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clear();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findOneByIdUserId(userId, id);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findOneByIndex(index);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findOneByIndexByUserId(userId, index);
        }
    }

    @Override
    @SneakyThrows
    public int remove(@Nullable final TaskDTO model) {
        if (model == null) throw new ModelNotFoundException();
        int result = 0;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            result = repository.remove(model);
            return result;
        }
    }

    @Override
    public void removeAll(@Nullable Collection<TaskDTO> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAll(collection);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int removeByUserId(@Nullable String userId, @Nullable TaskDTO model) {
        if (model == null) throw new ModelNotFoundException();
        int result = 0;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            result = repository.removeByUserId(userId, model);
            return result;
        }
    }

    @Override
    @SneakyThrows
    public int removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        int result = 0;
        @Nullable final TaskDTO task;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();

        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(id);
            if (task == null) return result;
            result = repository.remove(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public int removeByIdByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        int result = 0;

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable TaskDTO task;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIdUserId(userId, id);
            if (task == null) return result;
            result = repository.remove(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public int removeByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();

        int result = 0;
        @Nullable final TaskDTO task;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();

        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIndex(index);
            if (task == null) return result;
            result = repository.remove(task);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public int removeByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        int result = 0;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable TaskDTO task;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIndexByUserId(userId, index);
            if (task == null) return result;
            result = repository.remove(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public long count() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.count();
        }
    }

    @Override
    @SneakyThrows
    public long countByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.countByUserId(userId);
        }
    }

    @Override
    @SneakyThrows
    public int update(@Nullable final TaskDTO model) {
        int result = 0;
        if (model == null) return result;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserOwnerRepository<TaskDTO> repository = sqlSession.getMapper(ITaskRepository.class);
            result = repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable TaskDTO task;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIdUserId(userId, id);
            if (task == null) throw new ModelNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable TaskDTO task;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIndexByUserId(userId, index);
            if (task == null) throw new ModelNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable TaskDTO result;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            result = repository.findOneByIdUserId(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable TaskDTO result;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            result = repository.findOneByIndexByUserId(userId, index);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }
}
