package ru.tsc.felofyanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.endpoint.IUserEndpoint;
import ru.tsc.felofyanov.tm.api.service.IServiceLocator;
import ru.tsc.felofyanov.tm.api.service.IUserService;
import ru.tsc.felofyanov.tm.dto.model.SessionDTO;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;
import ru.tsc.felofyanov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePassword(
            @WebParam(name = "request", partName = "request")
            @NotNull UserChangePasswordRequest request
    ) {
        @Nullable SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final UserDTO user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable final Role role = request.getRole();
        @Nullable final UserDTO user = getUserService().createWithRole(login, password, email, role);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse viewProfileUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request
    ) {
        @Nullable SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateProfileUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUpdateProfileRequest request
    ) {
        @Nullable SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final UserDTO user = getUserService().updateUser(userId, firstName, middleName, lastName);
        return new UserUpdateProfileResponse(user);
    }
}
