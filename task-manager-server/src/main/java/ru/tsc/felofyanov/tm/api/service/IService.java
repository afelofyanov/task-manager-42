package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.api.repository.IRepository;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

}
