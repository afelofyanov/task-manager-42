package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

public interface IProjectService extends IUserOwnerService<ProjectDTO> {

}
