package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.ICommandRepository;
import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null && name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null && argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }
}
