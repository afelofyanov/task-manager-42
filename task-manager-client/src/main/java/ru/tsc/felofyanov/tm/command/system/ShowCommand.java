package ru.tsc.felofyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ShowCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }
}
